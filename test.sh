#!/bin/sh

set -e -o pipefail

      # <numarDosar>string</numarDosar>
      # <obiectDosar>string</obiectDosar>
      # <numeParte>string</numeParte>

curl -vvv http://portalquery.just.ro/query.asmx \
     -X POST \
     -H "Content-Type: text/xml" \
     --data-binary @- <<EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <CautareDosare xmlns="portalquery.just.ro">
      <institutie>CurteadeApelBUCURESTI</institutie>
      <dataStart>2022-08-26T00:00:00</dataStart>
      <dataStop>2022-08-29T00:00:00</dataStop>
    </CautareDosare>
  </soap:Body>
</soap:Envelope>
EOF
