default:
	just --choose

# check format and compilation
check:
	cargo fmt --all
	cargo check --workspace

# debug build
debug: check
	cargo build

# release build
release: check
	cargo build --release --workspace

# clean files unknown to git
clean:
	git clean -dxf

# run clippy
clippy:
	cargo clippy --workspace

# try out the API with curl
test-curl:
	./test.sh > res.xml
	tidy -modify -quiet -wrap 0 -xml -indent --tidy-mark false res.xml

# fetch the service directory from the website
fetch-wdsl:
	curl -s "http://portalquery.just.ro/query.asmx?WSDL" --output wsdl.xml
	tidy -modify -quiet -wrap 0 -xml -indent --tidy-mark false wsdl.xml

# init the local dev database
initdb:
	pg_ctl init -D dbdata/

# start the local dev database
startdb:
	pg_ctl -D dbdata/ -w -o '-k /tmp' start

# start dev web server
startweb:
	cd web/ && iex -S mix phx.server

# fetch and compile web things
compileweb:
	#!/bin/sh
	set -euxo pipefail
	cd web/
	mix deps.get
	mix deps.compile
	mix compile

# build a podman image of the phoenix app
container:
	#!/bin/sh
	set -euxo pipefail
	cd web/
	podman build .
