use crate::{models, OrError};
use anyhow::{anyhow, bail, Context};
use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
use clap::{Parser, Subcommand};
use diesel::{
    prelude::*,
    upsert::{excluded, on_constraint},
};
use log::{error, info, warn};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

const MIN_INTERVAL_MINS: i64 = 10;

#[derive(Parser)]
pub struct Opts {
    #[clap(subcommand)]
    pub cmd: Cmds,

    #[clap(long, env = "DATABASE_URL")]
    pub database_url: String,

    #[clap(long)]
    pub store_db: bool,
}

#[derive(Subcommand)]
pub enum Cmds {
    DupaData(DupaDataOpts),
    DupaModificare(DupaModificareOpts),
}

#[derive(Parser)]
pub struct DupaDataOpts {
    /// Court in which the case was registered.  The argument is used
    /// to do a case-insentivie substring match in the list of vaild
    /// courts (e.g. JudecatoriaBACAU, TribunalulBACAU,
    /// CurteadeApelBACAU). If empty, will search all courts.
    #[clap(long)]
    pub curte: Option<String>,

    /// Date in which the case was registered.
    #[clap(long)]
    pub data_inregistrare: NaiveDate,

    /// If provided, will instead do a search for cases between
    /// `data_inregistrare` and `data_inregistrare_sfarsit` inclusive.
    #[clap(long)]
    pub data_inregistrare_sfarsit: Option<NaiveDate>,
}

#[derive(Parser)]
pub struct DupaModificareOpts {
    /// County in which the case was registered.  The argument is used
    /// to do a case-insentivie substring match in the list of vaild
    /// courts (e.g. JudecatoriaBACAU, TribunalulBACAU,
    /// CurteadeApelBACAU). If empty, will search all courts.
    #[clap(long)]
    pub curte: Option<String>,

    /// The date on or after which the case must have been changed.
    #[clap(long)]
    pub data_modificare: NaiveDate,

    /// If provided, will instead do a search for cases modified between
    /// `data_modificare` and `data_modificare_sfarsit` inclusive.
    #[clap(long)]
    pub data_modificare_sfarsit: Option<NaiveDate>,
}

pub fn run(opts: Opts) -> OrError<()> {
    let mut queries = make_queries(&opts)?;
    let mut num_failures = 0;
    let mut num_splits = 0;
    while let Some(mut q) = queries.pop() {
        let queries_len = queries.len();
        if q.retries_left <= 0 {
            bail!("Ran out of retries for {q:?}");
        } else if let Err(err) = run_query(&q, &mut queries, &opts) {
            error!("Query failed (will retry): {q:?}");
            error!("Failure reason: {err}");
            q.retries_left -= 1;
            num_failures += 1;
            queries.push(q);
            std::thread::sleep(std::time::Duration::from_millis(4000));
        } else if queries.len() > queries_len {
            num_splits += 1;
        }
    }
    info!("All done. Splits: {num_splits}. Failures: {num_failures}");
    Ok(())
}

fn run_query(q: &DosareQuery, queries: &mut Vec<DosareQuery>, opts: &Opts) -> OrError<()> {
    info!("[{} left] Sending query: {}", queries.len(), q.to_xml());
    let body: String = ureq::post("http://portalquery.just.ro/query.asmx")
        .set("Content-Type", "text/xml")
        .send_string(&q.to_xml())?
        .into_string()?;
    let body = body.replace("&#x0;", ""); // remove null character entities
    let dosare = match Dosar::from_raw(&body, q.version) {
        Ok(x) => x,
        Err(err) => {
            error!("Failed to deserialize {body}");
            bail!(err)
        }
    };
    info!("num_dosare = {}", dosare.len());
    if dosare.len() >= 1000 {
        warn!(
            "Suspcious number of cases files ({}). Subdividing query interval and trying again...",
            dosare.len()
        );
        queries.extend(q.clone().subdivide()?);
        Ok(())
    } else if opts.store_db {
        let mut conn = PgConnection::establish(&opts.database_url)
            .unwrap_or_else(|_| panic!("Error connecting to {}", opts.database_url));
        let dosares: Vec<_> = dosare
            .into_iter()
            .map(|d| {
                Ok(models::DosarToInsert {
                    numar: d
                        .numar
                        .clone()
                        .unwrap_or_else(|| Uuid::new_v4().to_string()),
                    institutie: d.institutie.clone(),
                    data: d.data,
                    data_modificare: d.data_modificare.date(),
                    participanti_uniti: d
                        .parti
                        .iter()
                        .map(|p| p.nume.clone().unwrap_or_default())
                        .collect::<Vec<_>>()
                        .join(" "),
                    blob: serde_json::to_value(&d)?,
                })
            })
            .collect::<Vec<OrError<_>>>()
            .into_iter()
            .collect::<OrError<Vec<_>>>()?;
        let rows: Vec<models::Dosar> = {
            use crate::schema::dosare::dsl::*;
            diesel::insert_into(dosare)
                .values(&dosares)
                .on_conflict(on_constraint("dosare_numar_institutie_key"))
                .do_update()
                .set((
                    data.eq(excluded(data)),
                    data_modificare.eq(excluded(data_modificare)),
                    participanti_uniti.eq(excluded(participanti_uniti)),
                    blob.eq(excluded(blob)),
                ))
                .get_results(&mut conn)?
        };
        info!("upserted {} rows", rows.len());
        Ok(())
    } else {
        for d in dosare {
            println!("{}", serde_json::to_string(&d)?);
        }
        Ok(())
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Dosar {
    pub parti: Vec<Parte>,
    pub sedinte: Vec<Sedinta>,
    pub cai_atac: Vec<CaleAtac>,
    pub numar: Option<String>,
    pub numar_vechi: Option<String>,
    pub data: NaiveDate,
    pub institutie: String,
    pub departament: Option<String>,
    pub categorie_caz: String,
    pub stadiu_procesual: String,
    pub obiect: Option<String>,
    pub data_modificare: NaiveDateTime,
    pub categorie_caz_nume: Option<String>,
    pub stadiu_procesual_nume: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Parte {
    pub nume: Option<String>,
    pub calitate_parte: Option<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Sedinta {
    pub complet: Option<String>,
    pub data: NaiveDate,
    pub ora: Option<String>,
    pub solutie: Option<String>,
    pub solutie_sumar: Option<String>,
    pub data_pronuntare: Option<NaiveDate>,
    pub document_sedinta: String,
    pub numar_document: Option<String>,
    pub data_document: Option<NaiveDate>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CaleAtac {
    pub data_declarare: NaiveDate,
    pub parte_declaratoare: Option<String>,
    pub tip_cale_atac: Option<String>,
}

impl Dosar {
    pub fn from_raw(body: &str, vsn: QueryVersion) -> OrError<Vec<Dosar>> {
        let dosare_raw = match vsn {
            QueryVersion::V1 => {
                let e: raw::Envelope = yaserde::de::from_str(body)
                    .map_err(|str| anyhow!("Failed to deserialize: {str}"))?;
                match e.body.cautare_dosare_response.cautare_dosare_result {
                    None => vec![],
                    Some(c) => c.dosare,
                }
            }
            QueryVersion::V2 => {
                let e: raw::Envelope2 = yaserde::de::from_str(body)
                    .map_err(|str| anyhow!("Failed to deserialize: {str}"))?;
                match e.body.cautare_dosare2_response.cautare_dosare2_result {
                    None => vec![],
                    Some(c) => c.dosare,
                }
            }
        };
        let mut dosare = vec![];
        for d in dosare_raw {
            let parti = d
                .parti
                .xs
                .into_iter()
                .map(|x| Parte {
                    nume: x.nume,
                    calitate_parte: x.calitate_parte,
                })
                .collect();
            let sedinte = {
                let mut res = vec![];
                for x in d.sedinte.xs {
                    res.push(Sedinta {
                        complet: x.complet,
                        data: parse_date(&x.data).context("Sedinta::data")?,
                        ora: x.ora,
                        solutie: x.solutie,
                        solutie_sumar: x.solutie_sumar,
                        data_pronuntare: if x.data_pronuntare.is_empty() {
                            None
                        } else {
                            Some(
                                parse_date(&x.data_pronuntare)
                                    .context("Sedinta::data_pronuntare")
                                    .context(x.data_pronuntare.clone())?,
                            )
                        },
                        document_sedinta: x.document_sedinta,
                        numar_document: x.numar_document,
                        data_document: if x.data_document.is_empty() {
                            None
                        } else {
                            Some(parse_date(&x.data_document).context("Sedinta::data_document")?)
                        },
                    });
                }
                res
            };
            let cai_atac = {
                let mut res = vec![];
                for x in d.cai_atac.xs {
                    res.push(CaleAtac {
                        data_declarare: parse_date(&x.data_declarare)
                            .context("CaleAtac::data_declarare")?,
                        parte_declaratoare: x.parte_declaratoare,
                        tip_cale_atac: x.tip_cale_atac,
                    });
                }
                res
            };
            dosare.push(Dosar {
                parti,
                sedinte,
                cai_atac,
                numar: d.numar,
                numar_vechi: d.numar_vechi,
                data: parse_date(&d.data)
                    .context("Dosar::data")
                    .context(d.data.clone())?,
                institutie: d.institutie,
                departament: d.departament,
                categorie_caz: d.categorie_caz,
                stadiu_procesual: d.stadiu_procesual,
                obiect: d.obiect,
                data_modificare: parse_date_time(&d.data_modificare)
                    .context("Dosar::data_modificare")
                    .context(d.data_modificare.clone())?,
                categorie_caz_nume: d.categorie_caz_nume,
                stadiu_procesual_nume: d.stadiu_procesual_nume,
            })
        }
        Ok(dosare)
    }
}

const COURTS: &[&str] = &[
    "CurteadeApelBUCURESTI",
    "TribunalulBUCURESTI",
    "JudecatoriaSECTORUL4BUCURESTI",
    "TribunalulTIMIS",
    "CurteadeApelBACAU",
    "CurteadeApelCLUJ",
    "CurteadeApelORADEA",
    "CurteadeApelCONSTANTA",
    "CurteadeApelSUCEAVA",
    "TribunalulBOTOSANI",
    "CurteadeApelPLOIESTI",
    "CurteadeApelTARGUMURES",
    "CurteadeApelGALATI",
    "CurteadeApelIASI",
    "CurteadeApelPITESTI",
    "CurteadeApelCRAIOVA",
    "JudecatoriaARAD",
    "CurteadeApelALBAIULIA",
    "CurteadeApelTIMISOARA",
    "TribunalulBRASOV",
    "TribunalulDOLJ",
    "CurteadeApelBRASOV",
    "CurteaMilitaradeApelBUCURESTI",
    "TribunalulSATUMARE",
    "TribunalulSALAJ",
    "TribunalulSIBIU",
    "TribunalulSUCEAVA",
    "TribunalulTELEORMAN",
    "TribunalulTULCEA",
    "TribunalulVASLUI",
    "TribunalulVALCEA",
    "TribunalulVRANCEA",
    "TribunalulMilitarBUCURESTI",
    "TribunalulILFOV",
    "JudecatoriaBUFTEA",
    "TribunalulGORJ",
    "TribunalulHARGHITA",
    "TribunalulHUNEDOARA",
    "TribunalulIALOMITA",
    "TribunalulIASI",
    "TribunalulMARAMURES",
    "TribunalulMEHEDINTI",
    "TribunalulMURES",
    "TribunalulNEAMT",
    "TribunalulOLT",
    "TribunalulPRAHOVA",
    "TribunalulALBA",
    "TribunalulARAD",
    "TribunalulARGES",
    "TribunalulBACAU",
    "TribunalulBIHOR",
    "TribunalulBISTRITANASAUD",
    "TribunalulBRAILA",
    "TribunalulBUZAU",
    "TribunalulCARASSEVERIN",
    "TribunalulCALARASI",
    "TribunalulCLUJ",
    "TribunalulCONSTANTA",
    "TribunalulCOVASNA",
    "TribunalulDAMBOVITA",
    "TribunalulGALATI",
    "TribunalulGIURGIU",
    "JudecatoriaADJUD",
    "JudecatoriaAGNITA",
    "JudecatoriaAIUD",
    "JudecatoriaALBAIULIA",
    "JudecatoriaALESD",
    "JudecatoriaBABADAG",
    "JudecatoriaBACAU",
    "JudecatoriaBAIADEARAMA",
    "JudecatoriaBAIAMARE",
    "JudecatoriaBAILESTI",
    "JudecatoriaBALS",
    "JudecatoriaBALCESTI",
    "JudecatoriaBECLEAN",
    "JudecatoriaBEIUS",
    "JudecatoriaBICAZ",
    "JudecatoriaBARLAD",
    "JudecatoriaBISTRITA",
    "JudecatoriaBLAJ",
    "JudecatoriaBOLINTINVALE",
    "JudecatoriaBOTOSANI",
    "JudecatoriaBOZOVICI",
    "JudecatoriaBRAD",
    "JudecatoriaBRAILA",
    "JudecatoriaBRASOV",
    "JudecatoriaBREZOI",
    "JudecatoriaBUHUSI",
    "JudecatoriaBUZAU",
    "JudecatoriaCALAFAT",
    "JudecatoriaCALARASI",
    "JudecatoriaCAMPENI",
    "JudecatoriaCAMPINA",
    "JudecatoriaCAMPULUNG",
    "JudecatoriaCAMPULUNGMOLDOVENESC",
    "JudecatoriaCARACAL",
    "JudecatoriaCARANSEBES",
    "JudecatoriaCHISINEUCRIS",
    "JudecatoriaCLUJNAPOCA",
    "JudecatoriaCONSTANTA",
    "JudecatoriaCORABIA",
    "JudecatoriaCOSTESTI",
    "JudecatoriaCRAIOVA",
    "JudecatoriaCURTEADEARGES",
    "JudecatoriaDarabani",
    "JudecatoriaCAREI",
    "JudecatoriaDEJ",
    "JudecatoriaDETA",
    "JudecatoriaDEVA",
    "JudecatoriaDOROHOI",
    "JudecatoriaDRAGASANI",
    "JudecatoriaDRAGOMIRESTI",
    "JudecatoriaDROBETATURNUSEVERIN",
    "JudecatoriaFAGARAS",
    "JudecatoriaFALTICENI",
    "JudecatoriaFAUREI",
    "JudecatoriaFETESTI",
    "JudecatoriaFILIASI",
    "JudecatoriaFOCSANI",
    "JudecatoriaGAESTI",
    "JudecatoriaGALATI",
    "JudecatoriaGHEORGHENI",
    "JudecatoriaGHERLA",
    "JudecatoriaGIURGIU",
    "JudecatoriaGURAHUMORULUI",
    "JudecatoriaGURAHONT",
    "JudecatoriaHARLAU",
    "JudecatoriaHATEG",
    "JudecatoriaHOREZU",
    "JudecatoriaHUEDIN",
    "JudecatoriaHUNEDOARA",
    "JudecatoriaHUSI",
    "JudecatoriaIASI",
    "JudecatoriaINEU",
    "JudecatoriaINSURATEI",
    "JudecatoriaINTORSURABUZAULUI",
    "JudecatoriaLEHLIUGARA",
    "JudecatoriaLIPOVA",
    "JudecatoriaLUDUS",
    "JudecatoriaLUGOJ",
    "JudecatoriaMACIN",
    "JudecatoriaMANGALIA",
    "JudecatoriaMARGHITA",
    "JudecatoriaMEDGIDIA",
    "JudecatoriaMEDIAS",
    "JudecatoriaMIERCUREACIUC",
    "JudecatoriaMIZIL",
    "JudecatoriaMOINESTI",
    "JudecatoriaMOLDOVANOUA",
    "JudecatoriaMORENI",
    "JudecatoriaMOTRU",
    "JudecatoriaMURGENI",
    "JudecatoriaNASAUD",
    "JudecatoriaNEGRESTIOAS",
    "JudecatoriaNOVACI",
    "JudecatoriaODORHEIULSECUIESC",
    "JudecatoriaOLTENITA",
    "JudecatoriaONESTI",
    "JudecatoriaORADEA",
    "JudecatoriaORASTIE",
    "JudecatoriaORAVITA",
    "JudecatoriaORSOVA",
    "JudecatoriaPANCIU",
    "JudecatoriaPATARLAGELE",
    "JudecatoriaPETROSANI",
    "JudecatoriaPIATRANEAMT",
    "JudecatoriaPITESTI",
    "JudecatoriaPLOIESTI",
    "JudecatoriaPOGOANELE",
    "JudecatoriaPUCIOASA",
    "JudecatoriaRACARI",
    "JudecatoriaRADAUTI",
    "JudecatoriaRADUCANENI",
    "JudecatoriaRAMNICUSARAT",
    "JudecatoriaRAMNICUVALCEA",
    "JudecatoriaREGHIN",
    "JudecatoriaRESITA",
    "JudecatoriaROMAN",
    "JudecatoriaROSIORIDEVEDE",
    "JudecatoriaRUPEA",
    "JudecatoriaSALISTE",
    "JudecatoriaSANNICOLAULMARE",
    "JudecatoriaSATUMARE",
    "JudecatoriaSAVENI",
    "JudecatoriaSEBES",
    "JudecatoriaSECTORUL1BUCURESTI",
    "JudecatoriaSECTORUL2BUCURESTI",
    "JudecatoriaSECTORUL3BUCURESTI",
    "JudecatoriaSECTORUL5BUCURESTI",
    "JudecatoriaSECTORUL6BUCURESTI",
    "JudecatoriaSEGARCEA",
    "JudecatoriaSFANTUGHEORGHE",
    "JudecatoriaSIBIU",
    "JudecatoriaSIGHETUMARMATIEI",
    "JudecatoriaSIGHISOARA",
    "JudecatoriaSIMLEULSILVANIEI",
    "JudecatoriaSINAIA",
    "JudecatoriaSLATINA",
    "JudecatoriaSLOBOZIA",
    "JudecatoriaSTREHAIA",
    "JudecatoriaSUCEAVA",
    "JudecatoriaTARGOVISTE",
    "JudecatoriaTARGUBUJOR",
    "JudecatoriaTARGUCARBUNESTI",
    "JudecatoriaTARGUJIU",
    "JudecatoriaTARGULAPUS",
    "JudecatoriaTARGUMURES",
    "JudecatoriaTARGUNEAMT",
    "JudecatoriaTARGUSECUIESC",
    "JudecatoriaTARNAVENI",
    "JudecatoriaTECUCI",
    "JudecatoriaTIMISOARA",
    "JudecatoriaTOPLITA",
    "JudecatoriaTULCEA",
    "JudecatoriaTURDA",
    "JudecatoriaTURNUMAGURELE",
    "JudecatoriaURZICENI",
    "JudecatoriaVALENIIDEMUNTE",
    "JudecatoriaVANJUMARE",
    "JudecatoriaVASLUI",
    "JudecatoriaVATRADORNEI",
    "JudecatoriaVIDELE",
    "JudecatoriaVISEUDESUS",
    "JudecatoriaZALAU",
    "JudecatoriaZARNESTI",
    "JudecatoriaZIMNICEA",
    "TribunalulMilitarIASI",
    "JudecatoriaALEXANDRIA",
    "TribunalulMilitarTIMISOARA",
    "TribunalulMilitarCLUJNAPOCA",
    "TribunalulMilitarTeritorialBUCURESTI",
    "JudecatoriaAVRIG",
    "JudecatoriaTOPOLOVENI",
    "JudecatoriaPODUTURCULUI",
    "JudecatoriaFAGET",
    "JudecatoriaSALONTA",
    "JudecatoriaLIESTI",
    "JudecatoriaHARSOVA",
    "JudecatoriaSOMCUTAMARE",
    "JudecatoriaPASCANI",
    "TribunalulComercialARGES",
    "TribunalulComercialCLUJ",
    "TribunalulComercialMURES",
    "TribunalulpentruminoriSifamilieBRASOV",
    "JudecatoriaCORNETU",
    "JudecatoriaJIBOU",
];

/// Create the query XMLs to send to `portalquery.just.ro` for the
/// given options.  If `court` matches more than one valid court, then
/// multiple query XMLs will be returned.
fn make_queries(opts: &Opts) -> OrError<Vec<DosareQuery>> {
    match &opts.cmd {
        Cmds::DupaData(DupaDataOpts {
            curte,
            data_inregistrare,
            data_inregistrare_sfarsit,
        }) => {
            let data_start = data_inregistrare;
            let data_end = if let Some(data_inregistrare_sfarsit) = data_inregistrare_sfarsit {
                data_inregistrare_sfarsit.succ()
            } else {
                data_start.succ()
            };
            let courts = select_courts(curte)?;
            Ok(courts
                .into_iter()
                .map(|court| DosareQuery {
                    version: QueryVersion::V1,
                    institutie: court,
                    data_start: data_start.and_time(NaiveTime::from_hms(0, 0, 0)),
                    data_end: data_end.and_time(NaiveTime::from_hms(0, 0, 0)),
                    retries_left: 3,
                })
                .collect::<Vec<_>>())
        }
        Cmds::DupaModificare(DupaModificareOpts {
            curte,
            data_modificare,
            data_modificare_sfarsit,
        }) => {
            let data_start = data_modificare;
            let data_end = if let Some(data_modificare_sfarsit) = data_modificare_sfarsit {
                data_modificare_sfarsit.succ()
            } else {
                data_start.succ()
            };
            let courts = select_courts(curte)?;
            Ok(courts
                .into_iter()
                .map(|court| DosareQuery {
                    version: QueryVersion::V2,
                    institutie: court,
                    data_start: data_start.and_time(NaiveTime::from_hms(0, 0, 0)),
                    data_end: data_end.and_time(NaiveTime::from_hms(0, 0, 0)),
                    retries_left: 3,
                })
                .collect::<Vec<_>>())
        }
    }
}

fn select_courts(judet: &Option<String>) -> OrError<Vec<String>> {
    use regex::{Regex, RegexBuilder};
    let re = if let Some(judet) = judet {
        if !judet.chars().all(|c| c.is_alphanumeric()) {
            bail!("Court must be alpha numeric: '{judet}'")
        }
        RegexBuilder::new(&format!(".*{}.*", judet.to_ascii_lowercase()))
            .case_insensitive(true)
            .build()
            .unwrap()
    } else {
        Regex::new(".*").unwrap()
    };
    Ok(COURTS
        .iter()
        .filter_map(|c| {
            if re.is_match(c) {
                Some(c.to_string())
            } else {
                None
            }
        })
        .collect::<Vec<_>>())
}

#[derive(Clone, Debug)]
struct DosareQuery {
    version: QueryVersion,
    institutie: String,
    data_start: NaiveDateTime,
    data_end: NaiveDateTime,
    retries_left: i8,
}

impl DosareQuery {
    fn to_xml(&self) -> String {
        match self.version {
            QueryVersion::V1 => {
                format!(
                    r###"<?xml version="1.0" encoding="utf-8"?>
                             <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                               <soap:Body>
                                 <CautareDosare xmlns="portalquery.just.ro">
                                   <institutie>{}</institutie>
                                   <dataStart>{}</dataStart>
                                   <dataStop>{}</dataStop>
                                 </CautareDosare>
                               </soap:Body>
                             </soap:Envelope>
                        "###,
                    self.institutie,
                    self.data_start.format("%Y-%m-%dT%H:%M:%S"),
                    self.data_end.format("%Y-%m-%dT%H:%M:%S")
                )
            }
            QueryVersion::V2 => {
                format!(
                    r###"<?xml version="1.0" encoding="utf-8"?>
                             <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                               <soap:Body>
                                 <CautareDosare2 xmlns="portalquery.just.ro">
                                   <institutie>{}</institutie>
                                   <dataStart>1900-01-01T00:00:00</dataStart>
                                   <dataStop>2099-12-31T00:00:00</dataStop>
                                   <dataUltimaModificareStart>{}</dataUltimaModificareStart>
                                   <dataUltimaModificareStop>{}</dataUltimaModificareStop>
                                 </CautareDosare2>
                               </soap:Body>
                             </soap:Envelope>
                        "###,
                    self.institutie,
                    self.data_start.format("%Y-%m-%dT%H:%M:%S"),
                    self.data_end.format("%Y-%m-%dT%H:%M:%S")
                )
            }
        }
    }

    fn subdivide(self) -> OrError<Vec<Self>> {
        let delta = self.data_end.signed_duration_since(self.data_start) / 2;
        if delta.num_minutes() <= MIN_INTERVAL_MINS {
            bail!("Cannot subdivide duration {self:?}. Too small.");
        }
        let midpoint = self.data_start.checked_add_signed(delta).expect(
            "subdividing interval can't fail because we the old upper bound didn't overflow",
        );
        Ok(vec![
            Self {
                version: self.version,
                institutie: self.institutie.clone(),
                data_start: self.data_start,
                data_end: midpoint,
                retries_left: 3,
            },
            Self {
                version: self.version,
                institutie: self.institutie,
                data_start: midpoint,
                data_end: self.data_end,
                retries_left: 3,
            },
        ])
    }
}

fn parse_date(str: &str) -> OrError<NaiveDate> {
    Ok(NaiveDate::parse_from_str(str, "%Y-%m-%dT%H:%M:%S")?)
}

fn parse_date_time(str: &str) -> OrError<NaiveDateTime> {
    match NaiveDateTime::parse_from_str(str, "%Y-%m-%dT%H:%M:%S.%f") {
        Ok(dt) => Ok(dt),
        Err(_) => Ok(NaiveDateTime::parse_from_str(str, "%Y-%m-%dT%H:%M:%S")?),
    }
}

#[derive(Clone, Copy, Debug)]
pub enum QueryVersion {
    V1,
    V2,
}

pub mod raw {
    use yaserde_derive::YaDeserialize;

    #[derive(Debug, Default, YaDeserialize)]
    #[yaserde(
        root = "soap:Envelope",
        namespace = "http://schemas.xmlsoap.org/soap/envelope/"
    )]
    pub struct Envelope {
        #[yaserde(rename = "Body")]
        pub body: Body,
    }

    #[derive(Debug, Default, YaDeserialize)]
    pub struct Body {
        #[yaserde(rename = "CautareDosareResponse")]
        pub cautare_dosare_response: CautareDosareResponse,
    }

    #[derive(Debug, Default, YaDeserialize)]
    pub struct CautareDosareResponse {
        #[yaserde(rename = "CautareDosareResult")]
        pub cautare_dosare_result: Option<CautareDosareResult>,
    }

    #[derive(Debug, Default, YaDeserialize)]
    pub struct CautareDosareResult {
        #[yaserde(rename = "Dosar")]
        pub dosare: Vec<Dosar>,
    }

    #[derive(Debug, Default, YaDeserialize)]
    #[yaserde(
        root = "soap:Envelope",
        namespace = "http://schemas.xmlsoap.org/soap/envelope/"
    )]
    pub struct Envelope2 {
        #[yaserde(rename = "Body")]
        pub body: Body2,
    }

    #[derive(Debug, Default, YaDeserialize)]
    pub struct Body2 {
        #[yaserde(rename = "CautareDosare2Response")]
        pub cautare_dosare2_response: CautareDosare2Response,
    }

    #[derive(Debug, Default, YaDeserialize)]
    pub struct CautareDosare2Response {
        #[yaserde(rename = "CautareDosare2Result")]
        pub cautare_dosare2_result: Option<CautareDosareResult>,
    }

    #[derive(Debug, Default, YaDeserialize)]
    #[yaserde(namespace = "portalquery.just.ro")]
    pub struct Dosar {
        pub parti: Parti,
        pub sedinte: Sedinte,
        #[yaserde(rename = "caiAtac")]
        pub cai_atac: CaiAtac,
        pub numar: Option<String>,
        #[yaserde(rename = "numarVechi")]
        pub numar_vechi: Option<String>,
        pub data: String,
        pub institutie: String,
        pub departament: Option<String>,
        #[yaserde(rename = "categorieCaz")]
        pub categorie_caz: String,
        #[yaserde(rename = "stadiuProcesual")]
        pub stadiu_procesual: String,
        pub obiect: Option<String>,
        #[yaserde(rename = "dataModificare")]
        pub data_modificare: String,
        #[yaserde(rename = "categorieCazNume")]
        pub categorie_caz_nume: Option<String>,
        #[yaserde(rename = stadiuProcesualNume)]
        pub stadiu_procesual_nume: Option<String>,
    }

    #[derive(Debug, Default, YaDeserialize)]
    pub struct Parti {
        #[yaserde(rename = "DosarParte")]
        pub xs: Vec<DosarParte>,
    }

    #[derive(Debug, Default, YaDeserialize)]
    #[yaserde(namespace = "portalquery.just.ro")]
    pub struct DosarParte {
        pub nume: Option<String>,
        #[yaserde(rename = "calitateParte")]
        pub calitate_parte: Option<String>,
    }

    #[derive(Debug, Default, YaDeserialize)]
    pub struct Sedinte {
        #[yaserde(rename = "DosarSedinta")]
        pub xs: Vec<DosarSedinta>,
    }

    #[derive(Debug, Default, YaDeserialize)]
    #[yaserde(namespace = "portalquery.just.ro")]
    pub struct DosarSedinta {
        pub complet: Option<String>,
        pub data: String, // should be NaiveDate
        pub ora: Option<String>,
        pub solutie: Option<String>,
        #[yaserde(rename = "solutieSumar")]
        pub solutie_sumar: Option<String>,
        #[yaserde(rename = "dataPronuntare")]
        pub data_pronuntare: String,
        #[yaserde(rename = "documentSedinta")]
        pub document_sedinta: String,
        #[yaserde(rename = "numarDocument")]
        pub numar_document: Option<String>,
        #[yaserde(rename = "dataDocument")]
        pub data_document: String,
    }

    #[derive(Debug, Default, YaDeserialize)]
    pub struct CaiAtac {
        #[yaserde(rename = "DosarCaleAtac")]
        pub xs: Vec<DosarCaleAtac>,
    }

    #[derive(Debug, Default, YaDeserialize)]
    #[yaserde(namespace = "portalquery.just.ro")]
    pub struct DosarCaleAtac {
        #[yaserde(rename = "dataDeclarare")]
        pub data_declarare: String,
        #[yaserde(rename = "parteDeclaratoare")]
        pub parte_declaratoare: Option<String>,
        #[yaserde(rename = "tipCaleAtac")]
        pub tip_cale_atac: Option<String>,
    }
}
