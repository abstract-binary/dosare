use crate::{cautare_dosare, models::CompleteSyncToInsert, OrError};
use chrono::{NaiveDate, Utc};
use clap::Parser;
use diesel::prelude::*;
use log::info;

#[derive(Parser)]
pub struct Opts {
    #[clap(long, env = "DATABASE_URL")]
    database_url: String,

    #[clap(long)]
    start_date: Option<NaiveDate>,

    #[clap(long)]
    end_date: Option<NaiveDate>,
}

pub fn run(
    Opts {
        database_url,
        start_date,
        end_date,
    }: Opts,
) -> OrError<()> {
    let end_date = end_date.unwrap_or_else(|| Utc::today().naive_utc());
    let start_date = start_date.unwrap_or_else(|| end_date.pred());
    info!("Running daily sync between {start_date} and {end_date}");
    let opts = cautare_dosare::Opts {
        database_url: database_url.clone(),
        store_db: true,
        cmd: cautare_dosare::Cmds::DupaModificare(cautare_dosare::DupaModificareOpts {
            curte: None,
            data_modificare: start_date,
            data_modificare_sfarsit: Some(end_date),
        }),
    };
    cautare_dosare::run(opts)?;
    {
        use crate::schema::complete_syncs::dsl::*;
        let mut conn = PgConnection::establish(&database_url)
            .unwrap_or_else(|_| panic!("Error connecting to {}", database_url));
        diesel::insert_into(complete_syncs)
            .values(&CompleteSyncToInsert {
                most_recent_date: end_date,
            })
            .execute(&mut conn)?;
        info!("Added complete sync for {end_date}");
    };
    Ok(())
}
