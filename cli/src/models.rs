use crate::{schema::complete_syncs, schema::dosare, Json};
use chrono::{DateTime, NaiveDate, Utc};
use diesel::prelude::*;

#[derive(Queryable, Insertable)]
#[diesel(table_name = dosare)]
pub struct DosarToInsert {
    pub numar: String,
    pub institutie: String,
    pub data: NaiveDate,
    pub data_modificare: NaiveDate,
    pub participanti_uniti: String,
    pub blob: Json,
}

#[derive(Queryable, Insertable)]
#[diesel(table_name = dosare)]
pub struct Dosar {
    pub id: i32,
    pub numar: String,
    pub institutie: String,
    pub data: NaiveDate,
    pub data_modificare: NaiveDate,
    pub participanti_uniti: String,
    pub blob: Json,
    pub entry_time: DateTime<Utc>,
}

#[derive(Queryable, Insertable)]
#[diesel(table_name = complete_syncs)]
pub struct CompleteSyncToInsert {
    pub most_recent_date: NaiveDate,
}
