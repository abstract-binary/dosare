table! {
    complete_syncs (id) {
        id -> Int4,
        most_recent_date -> Date,
        entry_time -> Nullable<Timestamptz>,
    }
}

table! {
    dosare (id) {
        id -> Int4,
        numar -> Text,
        institutie -> Text,
        data -> Date,
        data_modificare -> Date,
        participanti_uniti -> Text,
        blob -> Jsonb,
        entry_time -> Timestamptz,
    }
}

table! {
    schema_migrations (version) {
        version -> Int8,
        inserted_at -> Nullable<Timestamp>,
    }
}

allow_tables_to_appear_in_same_query!(complete_syncs, dosare, schema_migrations,);
