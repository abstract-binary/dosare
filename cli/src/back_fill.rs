use std::{thread, time::Duration};

use crate::{cautare_dosare, OrError};
use anyhow::bail;
use chrono::NaiveDate;
use clap::Parser;
use log::{error, info};

const MAX_ATTEMPTS: u32 = 3;

#[derive(Parser)]
pub struct Opts {
    #[clap(long, env = "DATABASE_URL")]
    database_url: String,

    #[clap(long)]
    start_date: NaiveDate,

    #[clap(long)]
    end_date: NaiveDate,
}

pub fn run(
    Opts {
        database_url,
        start_date,
        end_date,
    }: Opts,
) -> OrError<()> {
    if start_date > end_date {
        bail!("start-date must be before or equal to end-date");
    }
    info!("Running back-fill between {start_date} and {end_date}");
    let end_date = end_date.succ();
    for date in start_date.iter_days().take_while(|d| d != &end_date) {
        let mut attempt = 0;
        info!("Back-filling {date} (attempt {attempt})");
        while attempt < MAX_ATTEMPTS {
            let opts = cautare_dosare::Opts {
                database_url: database_url.clone(),
                store_db: true,
                cmd: cautare_dosare::Cmds::DupaData(cautare_dosare::DupaDataOpts {
                    curte: None,
                    data_inregistrare: date,
                    data_inregistrare_sfarsit: Some(date),
                }),
            };
            match cautare_dosare::run(opts) {
                Ok(()) => break,
                Err(err) => {
                    error!("Failted to back-fill {date}: {err}");
                    thread::sleep(Duration::from_secs(5 * 60));
                    attempt += 1;
                }
            }
        }
        if attempt == MAX_ATTEMPTS {
            bail!("Permanently failed to back-fill {date}");
        } else {
            info!("Done back-filling {date}. Sleeping for 1min.");
            thread::sleep(Duration::from_secs(60));
        }
    }
    Ok(())
}
