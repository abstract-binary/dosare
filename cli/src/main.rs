use clap::{Parser, Subcommand};
use log::LevelFilter;
use simplelog::{ColorChoice, ConfigBuilder, TermLogger, TerminalMode};

type Json = serde_json::Value;
type OrError<T> = Result<T, anyhow::Error>;

#[macro_use]
extern crate diesel;

mod back_fill;
mod cautare_dosare;
mod daily_sync;
mod models;
mod schema;

#[derive(Parser)]
struct Opts {
    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Subcommand)]
enum Cmd {
    CautaDosare(cautare_dosare::Opts),
    DailySync(daily_sync::Opts),
    BackFill(back_fill::Opts),
}

fn main() -> OrError<()> {
    TermLogger::init(
        LevelFilter::Trace,
        ConfigBuilder::new()
            .add_filter_ignore_str("yaserde_derive")
            .add_filter_ignore_str("yaserde::de")
            .build(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    )?;
    let opts = Opts::parse();
    match opts.cmd {
        Cmd::CautaDosare(x) => cautare_dosare::run(x)?,
        Cmd::DailySync(x) => daily_sync::run(x)?,
        Cmd::BackFill(x) => back_fill::run(x)?,
    }
    Ok(())
}
