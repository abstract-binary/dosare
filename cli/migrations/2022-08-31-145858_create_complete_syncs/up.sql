create table complete_syncs (
  id serial primary key,
  most_recent_date date not null,
  entry_time timestamp with time zone default now()
);
