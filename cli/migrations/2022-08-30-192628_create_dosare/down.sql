-- This file should undo anything in `up.sql`

drop index dosare_numar_idx;
drop index dosare_participanti_idx;
drop index dosare_institutie_idx;
drop index dosare_data_idx;
drop index dosare_data_modificare_idx;
drop table dosare;
