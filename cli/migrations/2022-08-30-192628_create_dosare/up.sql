-- Your SQL goes here

create table dosare (
  id serial primary key,
  numar text not null,
  institutie text not null,
  data date not null,
  data_modificare date not null,
  participanti_uniti text not null,
  blob jsonb not null
);

create index dosare_numar_idx on dosare (numar);
create index dosare_institutie_idx on dosare (institutie);
create index dosare_data_idx on dosare (data);
create index dosare_data_modificare_idx on dosare (data_modificare);
create index dosare_participanti_idx on dosare using gin (to_tsvector('simple', participanti_uniti));
alter table dosare add unique (numar, institutie);
