defmodule DosareWeb.SearchLive do
  use DosareWeb, :live_view

  def mount(params, _uri, socket) do
    mrcs = Dosare.Just.most_recent_complete_sync()

    {:ok,
     socket
     |> assign(:most_recent_complete_sync, mrcs)
     |> assign(:modification_date_to, mrcs)
     |> parse_params(params)
     |> load_rows()}
  end

  def handle_params(params, _uri, socket) do
    {:noreply,
     socket
     |> parse_params(params)}
  end

  def handle_event("search", %{"search" => search_params}, socket) do
    {:noreply,
     socket
     |> parse_params(search_params)
     |> load_rows()
     |> update_url()}
  end

  defp load_rows(socket) do
    rows =
      Dosare.Just.list_dosare_by_participant_query(
        socket.assigns.query |> String.split("\n", trim: true),
        socket.assigns.modification_date_from,
        socket.assigns.modification_date_to
      )

    assign(socket, :rows, rows)
  end

  defp update_url(socket) do
    push_patch(
      socket,
      to:
        Routes.live_path(socket, DosareWeb.SearchLive, %{
          query: socket.assigns.query,
          modification_date_from: socket.assigns.modification_date_from |> to_string()
        })
    )
  end

  defp parse_params(socket, params) do
    query =
      case params["query"] do
        nil -> ""
        str -> str
      end

    modification_date_from =
      case params["modification_date_from"] do
        nil -> default_modification_date_from(socket)
        str -> parse_modification_date_from(socket, str)
      end

    socket
    |> assign(:query, query)
    |> assign(:modification_date_from, modification_date_from)
  end

  defp parse_modification_date_from(socket, str) do
    case Date.from_iso8601(str) do
      {:ok, date} -> date
      {:error, _} -> default_modification_date_from(socket)
    end
  end

  defp default_modification_date_from(socket) do
    socket.assigns.modification_date_to |> Date.add(-1)
  end

  defp format_case_participant(%{
         parte: %{"nume" => nume},
         parte_rexs: rexs
       }) do
    matched_bytes =
      Enum.map(rexs, fn rex ->
        Enum.map(Regex.scan(rex, nume, return: :index), fn [_, {start, len}] ->
          start..(start + len - 1)
        end)
      end)
      |> Enum.concat()

    {_, template} =
      Enum.reduce(String.split(nume, ""), {0, []}, fn c, {offset, acc} ->
        str =
          if Enum.any?(matched_bytes, fn m -> offset in m end) do
            "><span class=\"bg-green-300\">#{c}</span"
          else
            "><span>#{c}</span"
          end

        offset = offset + byte_size(c)

        {offset, [str | acc]}
      end)

    template = template |> Enum.reverse() |> Enum.join("")

    template = "<span#{template}>"
    assigns = %{template: template}
    ~H"<%= raw @template %>"
  end
end
