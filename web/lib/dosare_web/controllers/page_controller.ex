defmodule DosareWeb.PageController do
  use DosareWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
