defmodule Dosare.Repo do
  use Ecto.Repo,
    otp_app: :dosare,
    adapter: Ecto.Adapters.Postgres
end
