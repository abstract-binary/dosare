defmodule Dosare.Just.Dosar do
  use Ecto.Schema
  import Ecto.Changeset

  schema "dosare" do
    field :blob, :map
    field :data, :date
    field :data_modificare, :date
    field :institutie, :string
    field :numar, :string
    field :participanti_uniti, :string
    field :entry_time, :utc_datetime
  end

  @doc false
  def changeset(dosar, attrs) do
    dosar
    |> cast(attrs, [:id, :numar, :institutie, :data, :data_modificare, :participanti_uniti, :blob])
    |> validate_required([
      :id,
      :numar,
      :institutie,
      :data,
      :data_modificare,
      :participanti_uniti,
      :blob
    ])
  end
end
