defmodule Dosare.Just.CompleteSyncs do
  use Ecto.Schema
  import Ecto.Changeset

  schema "complete_syncs" do
    field :entry_time, :utc_datetime
    field :most_recent_date, :date
  end

  @doc false
  def changeset(complete_syncs, attrs) do
    complete_syncs
    |> cast(attrs, [:id, :most_recent_date, :entry_time])
    |> validate_required([:id, :most_recent_date, :entry_time])
  end
end
