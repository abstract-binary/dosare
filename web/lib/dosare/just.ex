defmodule Dosare.Just do
  @moduledoc """
  The Just context.
  """

  import Ecto.Query, warn: false
  alias Dosare.Repo

  alias Dosare.Just.Dosar

  @doc """
  Returns the list of dosare.
  """
  def list_dosare do
    Repo.all(Dosar)
  end

  @doc """
  Gets a single dosar.
  """
  def get_dosar!(id), do: Repo.get!(Dosar, id)

  @doc """
  List case files whose participants match any of the given
  participants and were modified in the given interval.
  """
  def list_dosare_by_participant_query(participants, modification_date_from, modification_date_to) do
    participants
    |> Enum.map(fn p ->
      p = String.trim(p)

      from(dosar in Dosar,
        order_by: [desc: dosar.data_modificare, asc: dosar.numar],
        where:
          fragment("participanti_uniti @@ plainto_tsquery(?)", ^p) and
            dosar.data_modificare >= ^modification_date_from and
            dosar.data_modificare <= ^modification_date_to
      )
      |> Repo.all()
      |> Enum.flat_map(fn r ->
        rexs =
          Enum.map(String.split(p, ~r/[ ,.;-]/, trim: true), fn word ->
            Regex.compile!("(?<name>#{word})", "i")
          end)

        parte =
          Enum.find(r.blob["parti"], nil, fn %{"nume" => nume} ->
            Enum.all?(rexs, fn rex ->
              Regex.match?(rex, nume)
            end)
          end)

        case parte do
          nil ->
            []

          %{} ->
            [
              r
              |> Map.put(:participant, p)
              |> Map.put(:parte, parte)
              |> Map.put(:parte_rexs, rexs)
            ]
        end
      end)
    end)
    |> Enum.concat()
  end

  alias Dosare.Just.CompleteSyncs

  @doc """
  Returns the date of the most recently completed sync.  This is the
  upper bound that makes sense for a "record modified between X and Y"
  query.
  """
  def most_recent_complete_sync do
    from(c in CompleteSyncs,
      select: c.most_recent_date,
      order_by: [desc: c.most_recent_date],
      limit: 1
    )
    |> Repo.one()
  end
end
