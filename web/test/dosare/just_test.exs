defmodule Dosare.JustTest do
  use Dosare.DataCase

  alias Dosare.Just

  describe "dosare" do
    alias Dosare.Just.Dosar

    import Dosare.JustFixtures

    @invalid_attrs %{blob: nil, data: nil, data_modificare: nil, id: nil, institutie: nil, numar: nil, participanti_uniti: nil}

    test "list_dosare/0 returns all dosare" do
      dosar = dosar_fixture()
      assert Just.list_dosare() == [dosar]
    end

    test "get_dosar!/1 returns the dosar with given id" do
      dosar = dosar_fixture()
      assert Just.get_dosar!(dosar.id) == dosar
    end

    test "create_dosar/1 with valid data creates a dosar" do
      valid_attrs = %{blob: %{}, data: ~D[2022-08-30], data_modificare: ~D[2022-08-30], id: 42, institutie: "some institutie", numar: "some numar", participanti_uniti: "some participanti_uniti"}

      assert {:ok, %Dosar{} = dosar} = Just.create_dosar(valid_attrs)
      assert dosar.blob == %{}
      assert dosar.data == ~D[2022-08-30]
      assert dosar.data_modificare == ~D[2022-08-30]
      assert dosar.id == 42
      assert dosar.institutie == "some institutie"
      assert dosar.numar == "some numar"
      assert dosar.participanti_uniti == "some participanti_uniti"
    end

    test "create_dosar/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Just.create_dosar(@invalid_attrs)
    end

    test "update_dosar/2 with valid data updates the dosar" do
      dosar = dosar_fixture()
      update_attrs = %{blob: %{}, data: ~D[2022-08-31], data_modificare: ~D[2022-08-31], id: 43, institutie: "some updated institutie", numar: "some updated numar", participanti_uniti: "some updated participanti_uniti"}

      assert {:ok, %Dosar{} = dosar} = Just.update_dosar(dosar, update_attrs)
      assert dosar.blob == %{}
      assert dosar.data == ~D[2022-08-31]
      assert dosar.data_modificare == ~D[2022-08-31]
      assert dosar.id == 43
      assert dosar.institutie == "some updated institutie"
      assert dosar.numar == "some updated numar"
      assert dosar.participanti_uniti == "some updated participanti_uniti"
    end

    test "update_dosar/2 with invalid data returns error changeset" do
      dosar = dosar_fixture()
      assert {:error, %Ecto.Changeset{}} = Just.update_dosar(dosar, @invalid_attrs)
      assert dosar == Just.get_dosar!(dosar.id)
    end

    test "delete_dosar/1 deletes the dosar" do
      dosar = dosar_fixture()
      assert {:ok, %Dosar{}} = Just.delete_dosar(dosar)
      assert_raise Ecto.NoResultsError, fn -> Just.get_dosar!(dosar.id) end
    end

    test "change_dosar/1 returns a dosar changeset" do
      dosar = dosar_fixture()
      assert %Ecto.Changeset{} = Just.change_dosar(dosar)
    end
  end

  describe "complete_syncs" do
    alias Dosare.Just.CompleteSyncs

    import Dosare.JustFixtures

    @invalid_attrs %{entry_time: nil, id: nil, most_recent_date: nil}

    test "list_complete_syncs/0 returns all complete_syncs" do
      complete_syncs = complete_syncs_fixture()
      assert Just.list_complete_syncs() == [complete_syncs]
    end

    test "get_complete_syncs!/1 returns the complete_syncs with given id" do
      complete_syncs = complete_syncs_fixture()
      assert Just.get_complete_syncs!(complete_syncs.id) == complete_syncs
    end

    test "create_complete_syncs/1 with valid data creates a complete_syncs" do
      valid_attrs = %{entry_time: ~T[14:00:00], id: 42, most_recent_date: ~D[2022-08-31]}

      assert {:ok, %CompleteSyncs{} = complete_syncs} = Just.create_complete_syncs(valid_attrs)
      assert complete_syncs.entry_time == ~T[14:00:00]
      assert complete_syncs.id == 42
      assert complete_syncs.most_recent_date == ~D[2022-08-31]
    end

    test "create_complete_syncs/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Just.create_complete_syncs(@invalid_attrs)
    end

    test "update_complete_syncs/2 with valid data updates the complete_syncs" do
      complete_syncs = complete_syncs_fixture()
      update_attrs = %{entry_time: ~T[15:01:01], id: 43, most_recent_date: ~D[2022-09-01]}

      assert {:ok, %CompleteSyncs{} = complete_syncs} = Just.update_complete_syncs(complete_syncs, update_attrs)
      assert complete_syncs.entry_time == ~T[15:01:01]
      assert complete_syncs.id == 43
      assert complete_syncs.most_recent_date == ~D[2022-09-01]
    end

    test "update_complete_syncs/2 with invalid data returns error changeset" do
      complete_syncs = complete_syncs_fixture()
      assert {:error, %Ecto.Changeset{}} = Just.update_complete_syncs(complete_syncs, @invalid_attrs)
      assert complete_syncs == Just.get_complete_syncs!(complete_syncs.id)
    end

    test "delete_complete_syncs/1 deletes the complete_syncs" do
      complete_syncs = complete_syncs_fixture()
      assert {:ok, %CompleteSyncs{}} = Just.delete_complete_syncs(complete_syncs)
      assert_raise Ecto.NoResultsError, fn -> Just.get_complete_syncs!(complete_syncs.id) end
    end

    test "change_complete_syncs/1 returns a complete_syncs changeset" do
      complete_syncs = complete_syncs_fixture()
      assert %Ecto.Changeset{} = Just.change_complete_syncs(complete_syncs)
    end
  end
end
