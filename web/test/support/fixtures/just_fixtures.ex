defmodule Dosare.JustFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Dosare.Just` context.
  """

  @doc """
  Generate a dosar.
  """
  def dosar_fixture(attrs \\ %{}) do
    {:ok, dosar} =
      attrs
      |> Enum.into(%{
        blob: %{},
        data: ~D[2022-08-30],
        data_modificare: ~D[2022-08-30],
        id: 42,
        institutie: "some institutie",
        numar: "some numar",
        participanti_uniti: "some participanti_uniti"
      })
      |> Dosare.Just.create_dosar()

    dosar
  end

  @doc """
  Generate a complete_syncs.
  """
  def complete_syncs_fixture(attrs \\ %{}) do
    {:ok, complete_syncs} =
      attrs
      |> Enum.into(%{
        entry_time: ~T[14:00:00],
        id: 42,
        most_recent_date: ~D[2022-08-31]
      })
      |> Dosare.Just.create_complete_syncs()

    complete_syncs
  end
end
